import { useState } from 'react';
import { actions, useAppDispatch } from '../../store';
import './upload-files.css';

export const UploadFiles = () => {

  const dispatch = useAppDispatch();

  const [drag, setDrag] = useState(false);

  function dragStartHandler(e: React.DragEvent<HTMLDivElement>): void {
    e.preventDefault();
    setDrag(true);
  }

  function dragLeaveHandler(e: React.DragEvent<HTMLDivElement>): void {
    e.preventDefault();
    setDrag(false);
  }

  async function onDropHandler(e: React.DragEvent<HTMLDivElement>): Promise<void> {
    e.preventDefault();

    const file = e.dataTransfer.files;
    const formData = new FormData();
    formData.append('file', file[0]);
    await dispatch(actions.fies.createFile(formData));
    setDrag(false);
  }

  return (
    <div className="upload_files_container">
      <h1 className='upload_files_title'>Загрузка файлов</h1>
      <div>
        {drag
          ? <div
            onDragStart={(e) => dragStartHandler(e)}
            onDragLeave={(e) => dragLeaveHandler(e)}
            onDragOver={(e) => dragStartHandler(e)}
            onDrop={(e) => onDropHandler(e)}
            className='drop_area'
          >Отпустите файл для загрузки</div>
          : <div
            onDragStart={(e) => dragStartHandler(e)}
            onDragLeave={(e) => dragLeaveHandler(e)}
            onDragOver={(e) => dragStartHandler(e)}
            className='non_drop_area'
          >Перетащите файл для загрузки</div>
        }
      </div>
    </div>
  );
}
