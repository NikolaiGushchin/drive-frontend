import { ComponentProps, FC } from "react";
import './input.css';

export interface InputProps extends ComponentProps<"input"> {}

export const Input: FC<InputProps> = ({
  value,
  onChange,
  type,
  placeholder,
}) => {
  return (
    <div className="input">
      <input
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        className="text-field__input"
      />
    </div>
  );
};
