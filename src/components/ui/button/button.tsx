import { ComponentProps, FC } from "react";
import './button.css';

export interface ButtonProps extends ComponentProps<"button"> {
  text: string;
}

export const Button: FC<ButtonProps> = ({
  text,
  onClick,
}) => {
  return (
    <button onClick={onClick} className="button">{text}</button>
  );
}
