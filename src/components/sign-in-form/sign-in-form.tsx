import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { actions, useAppDispatch } from "../../store";
import { Button } from "../ui/button/button";
import { Input } from "../ui/input/input";
import './sign-in-form.css';

export const SignInForm = () => {

  const nav = useNavigate();

  const dispatch = useAppDispatch();

  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const trySignIn = async () => {
    await dispatch(actions.auth.signIn({ email, password })).unwrap();
    nav('/files/');
  };
  
  return (
    <div className="sign-in-form">
      <div className="sign-in-title">Вход</div>
      <Input
        type="text"
        placeholder="Введите email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <Input
        type="password"
        placeholder="Введите пароль"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <Button
        text="Войти"
        onClick={trySignIn}
      />
    </div>
  );
}
