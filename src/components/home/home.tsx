import './home.css';

export const Home = () => {
  return(
    <div className='home'>
      <h1>Добро пожаловать!</h1>
      <h1>Для использования системы Вам необходимо войти или зарегестрироваться</h1>
    </div>
  );
}