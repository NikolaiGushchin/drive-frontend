import { ComponentProps, FC } from 'react';
import { BASE_API_PATH } from '../../api/constans';
import { actions, useAppDispatch } from '../../store';
import { Button } from '../ui/button/button';
import './file.css'

export interface FileProps extends ComponentProps<"div"> {
  fileUrl: string;
  fileId: string;
  fileName: string;
}

export const File: FC<FileProps> = ({
  fileUrl,
  fileId,
  fileName,
}) => {

  const dispatch = useAppDispatch();

  const tryDeleteFile = async () => {
    await dispatch(actions.fies.deleteFile(fileId));
  }

  const tryDownloadFile = async () => {
    await dispatch(actions.fies.downloadFile(fileUrl));
  }

  return (
    <div className='file_content'>
      <img src= {BASE_API_PATH + fileUrl} alt={fileName} className='img'/>
      <div className='file_text'>{fileName}</div>
      <div className='file_btn'>
        <Button
          text="Скачать"
          onClick={tryDownloadFile}
        />
        <Button
          text="Удалить"
          onClick={tryDeleteFile}
        />
      </div>
    </div>
  );
}
