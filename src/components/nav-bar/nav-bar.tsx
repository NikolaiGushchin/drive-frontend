import { ComponentProps, FC } from "react";
import { Link } from "react-router-dom";
import './nav-bar.css';

export interface NavProps extends ComponentProps<"div"> {
  isLogging: string | undefined;
}

export const NavBar: FC<NavProps> = ({ isLogging }) => {
  return (
    <div className="nav-bar">
      {
        isLogging
          ? (
              <div>
                <div className="right-nav">
                  <ul className="links">
                    <li>
                      <Link to='/files' className="link_button">Файлы</Link>
                    </li>
                    <li>
                      <Link to='/uploads' className="link_button">Загрузка файлов</Link>
                    </li>
                  </ul>
                </div>
              </div>
          )
          : (
            <div className="right-nav">
              <ul className="links">
                <li>
                  <Link to='/signUp' className="link_button">Регистрация</Link>
                </li>
                <li>
                  <Link to='/signIn' className="link_button">Войти</Link>
                </li>
              </ul>
            </div>
          )
      }
    </div>
  );
}