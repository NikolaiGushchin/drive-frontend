import { useEffect } from "react";
import { actions, selectors, useAppDispatch, useAppSelector } from "../../store";
import { File } from "../file/file";
import './files-list.css'

export const FilesList = () => {

  const { files } = useAppSelector(
    selectors.files.files
  );

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(actions.fies.getAllFiles());
  }, []);

  const renderFiles = () => {
    return files.map((file) => {
      return <File
        key={file.id}
        fileName={file.name.slice(37)}
        fileId={file.id}
        fileUrl={file.fileUrl}
      />
    });
  }

  return (
    <div className="files_list">
      {
        files.length
        ? (
          renderFiles()
        )
        : (
          <div className="files_not_found">
            <h1>У вас нет файлов</h1>
          </div>
        )
      }
    </div>
  );
}
