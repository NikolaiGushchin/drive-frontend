import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { actions, useAppDispatch } from "../../store";
import { Button } from "../ui/button/button";
import { Input } from "../ui/input/input";
import './sign-up-form.css';

export const SignUpForm = () => {

  const nav = useNavigate();

  const dispatch = useAppDispatch();

  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");


  const trySignUp = async () => {
    await dispatch(actions.auth.signUp({ email, password })).unwrap();
    nav('/files/');
  };

  return (
    <div className="sign-up-form">
      <div className="sign-up-title">Регистрация</div>
      <Input
        type="text"
        placeholder="Введите email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <Input
        type="password"
        placeholder="Введите пароль"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <Button
        text="Зарегистрироваться"
        onClick={trySignUp}
      />
    </div>
  );
}
