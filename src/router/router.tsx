import { useState } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { FilesList } from "../components/files-list/files-list";
import { Home } from "../components/home/home";
import { NavBar } from "../components/nav-bar/nav-bar";
import { SignInForm } from "../components/sign-in-form/sign-in-form";
import { SignUpForm } from "../components/sign-up-form/sign-up-form";
import { UploadFiles } from "../components/upload-files/upload-files";
import { selectors, useAppSelector } from "../store";
import './router.css';

export const RouterDom = () => {

  const { refreshToken } = useAppSelector(selectors.auth.auth);

  return (
    <div className="container">
      <NavBar isLogging={refreshToken}/>
      <Routes>
        <Route path='/' element={ <Home/> } />
        <Route path='/uploads' element={ <UploadFiles/> } />
        <Route path='/files' element={ <FilesList/> } />
        <Route path='/signUp' element={ <SignUpForm /> } />
        <Route path='/signIn' element={ <SignInForm /> } />
      </Routes>
    </div>
  );
}