import { httpClient } from "../config"
import { paths } from "../constans";
import { DeleteFileResponseDto, FileResponseDto } from "../types";

export const fetchAllFiles = async (): Promise<FileResponseDto[]> => {
  const res = await httpClient.get(paths.files);
  return res.data;
}

export const removeFile = async (id: string): Promise<DeleteFileResponseDto> => {
  const res = await httpClient.delete(paths.files + id);
  return res.data;
}

export const uploadFile = async (file: FormData): Promise<FileResponseDto> => {
  const res = await httpClient.post(paths.uploadsFile, file);
  return res.data;
}

export const downloadOneFile = async (fileUrl: string) => {
  await httpClient.get(paths.files + fileUrl, {responseType: 'blob'}).then(
    (res) => {
      
      const url = window.URL.createObjectURL(new Blob([res.data]));
      const link = document.createElement('a');
      link.href = url;
      link.download = fileUrl;
      document.body.appendChild(link);
      link.click();
      link.remove();
    }
  )
}
