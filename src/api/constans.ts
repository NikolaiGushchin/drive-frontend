export const BASE_API_PATH = "https://imgdrive.herokuapp.com/";

export enum paths {
  signIn = "auth/sign-in",
  signUp = "auth/sign-up",
  files = "files/",
  uploadsFile = "files/uploads/",
  dowloadFile = "files/uploads/",
}
