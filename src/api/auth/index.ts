import { httpClient } from "../config";
import { paths } from "../constans";
import { 
  SignInDto, 
  SignInResponseDto, 
  SignUpDto, 
  SignUpResponseDto 
} from "../types";

export const SignUp = async (data: SignUpDto): Promise<SignUpResponseDto> => {
  const res = await httpClient.post(paths.signUp, JSON.stringify(data));
  return res.data;
}

export const SignIn = async (data: SignInDto): Promise<SignInResponseDto> => {
  const res = await httpClient.post(paths.signIn, JSON.stringify(data));
  return res.data;
}
