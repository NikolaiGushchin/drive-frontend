export type SignInDto = {
  email: string,
  password: string,
}

export type SignInResponseDto = {
  userId: string,
  accessToken: string,
  refreshToken: string,
}

export type SignUpDto = {
  email: string,
  password: string,
}

export type SignUpResponseDto = {
  userId: string,
  accessToken: string,
  refreshToken: string,
}

export type FileResponseDto = {
  id: string;
  fileUrl: string;
  name: string;
  userId: string;
  updatedAt: string;
  createdAt: string;
}

export type DeleteFileResponseDto = {
  id: string;
}
