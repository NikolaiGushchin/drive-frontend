import axios, { AxiosRequestConfig } from "axios";
import { store } from "../../store";
import { BASE_API_PATH } from "../constans";

const config: AxiosRequestConfig = {
  baseURL: BASE_API_PATH,
  headers: {
    "access-control-allow-origin": "*",
    "Content-Type": "application/json",
  },
};

export const httpClient = axios.create(config);

httpClient.interceptors.request.use((config) => {
  const token = store.getState().auth.refreshToken;
  if (token) {
    config.headers!.Authorization = `Bearer ${token}`;
  }
  return config;
});
