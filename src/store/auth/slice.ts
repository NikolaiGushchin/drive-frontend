import { createReducer, PayloadAction } from "@reduxjs/toolkit";
import { SignInResponseDto, SignUpResponseDto } from "../../api/types";
import { authActions } from "./thunk";

export interface AuthState {
  userId?: string;
  accessToken?: string,
  refreshToken?: string,
};

const initialState: AuthState = {};

export const authReducer = createReducer(
  initialState, 
  (builder) => {
    builder
      .addCase(
        authActions.signUp.fulfilled.type,
        (state, { payload }: PayloadAction<SignUpResponseDto>) => {
          state.userId = payload.userId;
          state.accessToken = payload.accessToken;
          state.refreshToken = payload.refreshToken;
        }
      )
      .addCase(
        authActions.signIn.fulfilled.type,
        (state, { payload }: PayloadAction<SignInResponseDto>) => {
          state.userId = payload.userId;
          state.accessToken = payload.accessToken;
          state.refreshToken = payload.refreshToken;
        }
      )
  }
);