import { createAsyncThunk } from "@reduxjs/toolkit";
import { SignIn, SignUp } from "../../api/auth";
import { SignUpDto, SignUpResponseDto } from "../../api/types";

const signUp = createAsyncThunk<SignUpResponseDto, SignUpDto>(
  "signUp",
  async (data) => {
    try {
      return await SignUp(data);
    } catch (e) {
      throw e;
    }
  }
);

const signIn = createAsyncThunk<SignUpResponseDto, SignUpDto>(
  "signIn",
  async (data) => {
    try {
      return await SignIn(data);
    } catch (e) {
      throw e;
    }
  }
);

export const authActions = {
  signUp,
  signIn,
};
