import * as auth from "./auth";
import * as files from "./files";

export const actions = {
  auth: auth.authActions,
  fies: files.fileActions,
};

export const selectors = {
  auth: auth.selectors,
  files: files.selectors,
};
