import { RootState } from "../config";

export const selectors = {
  files: (state: RootState) => state.files,
};
