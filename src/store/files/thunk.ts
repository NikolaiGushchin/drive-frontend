import { createAsyncThunk } from "@reduxjs/toolkit";
import { downloadOneFile, fetchAllFiles, removeFile, uploadFile } from "../../api/files";
import { DeleteFileResponseDto, FileResponseDto } from "../../api/types";

const getAllFiles = createAsyncThunk<FileResponseDto[]>(
  "files",
  async () => {
    return await fetchAllFiles();
  }
);

const deleteFile = createAsyncThunk<DeleteFileResponseDto, string>(
  "deleteFiles",
  async (id: string) => {
    return await removeFile(id);
  }
);

const createFile = createAsyncThunk<FileResponseDto, FormData>(
  "createFile",
  async (file) => {
    return await uploadFile(file);
  }
);

const downloadFile = createAsyncThunk<any, any>(
  "downloadFile",
  async (fileUrl: string) => {
    return await downloadOneFile(fileUrl);
  }
)

export const fileActions = {
  getAllFiles,
  deleteFile,
  createFile,
  downloadFile,
}
