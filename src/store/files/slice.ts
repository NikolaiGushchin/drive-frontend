import { createReducer, PayloadAction } from "@reduxjs/toolkit";
import { DeleteFileResponseDto, FileResponseDto } from "../../api/types";
import { fileActions } from "./thunk";

export interface FileState {
  files: FileResponseDto[];
}

const initialState: FileState = {
  files: [],
};

export const filesReducer = createReducer(
  initialState,
  (builder) => {
    builder
      .addCase(
        fileActions.getAllFiles.fulfilled.type,
        (state, { payload }: PayloadAction<FileResponseDto[]>) => {
          state.files = payload;
        }
      )
      .addCase(
        fileActions.deleteFile.fulfilled.type,
        (state, { payload }: PayloadAction<DeleteFileResponseDto>) => {
          state.files = state.files.filter((file) => file.id !== payload.id);
        }
      )
      .addCase(
        fileActions.createFile.fulfilled.type,
        (state, { payload }: PayloadAction<FileResponseDto>) => {
          state.files.push(payload);
        }
      )
  }
);
