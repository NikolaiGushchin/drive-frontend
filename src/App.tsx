import { RouterDom } from "./router/router";
import './style.css';

export const App = () => {
  return (
    <RouterDom />
  );
}
